import json
import pandas as pd 
import copy
from collections import OrderedDict
from collections import defaultdict

def lambda_handler(event, context):
    json_result = data_fetch_json(event['body'])
    # json_result = event['body']
    return {
        'statusCode': 200,
        'body': json_result
    }

def data_fetch_json(json_result):
    dict_batch_data = {}
    list_inner_data = list()
    dict_batch_data['data'] = list()

    data = pd.read_json(json_result)

    data_inner = data['person'] ## data_inner stores everythings about person names, emails, jobs......

    #
    data_inner_keys = data_inner[0].keys()  ## fetching all keys of data

    #
    dict_header = {}  ## initializing header for keys in json
    #
    for header in data_inner_keys:
        dict_header[header] = 0  ## initializing the dictionary to 0
    #
    length_batch_data = len(data)

    #
    # ##Fetching all keys from dictionary because they are unique for all person
    for i in range(length_batch_data):
        data_inner = data['person'][i]

    #     ## Finding the maximum count of each key from whole bunch of json for all person's data
        for key, value in data_inner.items():
            length_key_data = 0
            try:
                if (type(data_inner[key]) is list):
                    length_key_data = len(data_inner[key])
                elif (type(data_inner[key]) is str):
                    length_key_data = 1
            except:
                length_key_data = 0

            if (dict_header[key] <= length_key_data):
                dict_header[key] = length_key_data


    custom_order_list_keys = custom_order_key(dict_header)



    # ## creating a dictionary with renaming the keys with no of occurance of the keys from (1-n) ex: job:{job_1,job_2,job_3,job_4...job_n}
    dict_header_items = dict()
    for key, value in dict_header.items():
        if(key=='urls'):
            for i in range(value):
                dict_header_items['social_'+str(key) + '_' + str(i + 1)] = ""
        elif(key == 'names'or key == 'dob' or key == 'gender' or key == 'UniqueId' or key == 'origin_countries'):
            dict_header_items[key]=""
        else:
            for i in range(value):
                dict_header_items[str(key) + '_' + str(i + 1)] = ""
            if(value == 0):
                dict_header_items[key] = ""
    #
    # ## we have three different exception in set of dictionaries keys name,gender,dob which are unique and cannot be multiple
    #
    for i in range(length_batch_data):

        dict_result = copy.deepcopy(dict_header_items) ## deep copy allows us to create new copy of dictionary(we need this copy coz tradiotnal copy methods also overwrite the original dictionary with copied object coz its on same reference location)
        dict_result.pop('ethnicities')
        dict_result.pop('origin_countries')
        dict_result.pop('UniqueId')

        dict_result['age'] = dict_result.pop('dob')

        dict_result['names'] = ''
        dict_result['gender'] = ''


    #
        data_inner = data['person'][i]

    #

        for key, value in data_inner.items():

            if (key == 'names'):

                try:

                    if(('last' not in data_inner[key][0].keys())):
                        dict_result['names'] = data_inner[key][0]['first']
                    elif(('first' not in data_inner[key][0].keys())):
                        dict_result['names'] = data_inner[key][0]['last']
                    else:
                        dict_result['names'] = data_inner[key][0]['first'] + " " + data_inner[key][0]['last']

                except:

                    dict_result['names'] = ""


            elif (key == 'dob'):
                try:
                    dict_result['age'] = data_inner['dob']['display']
                except:
                    dict_result['age'] = ""


            elif (key == 'gender'):
                try:

                    dict_result['gender'] = data_inner['gender']['content']
                except:
                    dict_result['gender'] = ""

            # elif (key == 'UniqueId'):
            #     try:
            #         dict_result['UniqueId'] = data_inner['UniqueId']
            #     except:
            #         dict_result['UniqueId'] = ""
            elif (key == 'UniqueId'):
                try:
                    dict_result['appID'] = data_inner['appID']
                except:
                    dict_result['appID'] = ""

            else:
                len_key = 0
                flag = 0
                try:
                    if(type(data_inner[key]) is list):
                        len_key = len(data_inner[key])
                        flag = 1
                    elif(type(data_inner[key]) is str):
                        len_key = 1
                except:
                    len_key = 0

                for i in range(len_key):
                    if(key=='emails'):
                        try:
                            if(len_key == 1 and flag == 0):
                                dict_result[str(key) + '_' + str(i + 1)] = data_inner[key]
                            elif(len_key == 1 and flag == 1):
                                dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['address']
                            else:
                                dict_result[str(key)+'_'+str(i+1)] = data_inner[key][i]['address']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif(key=='images'):
                        try:
                            dict_result[str(key)+'_'+str(i+1)] = data_inner[key][i]['url']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'usernames'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['content']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'phones'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display_international']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'languages'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'addresses'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'addresses'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'jobs'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'educations'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'relationships'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['names'][0]['display']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'user_ids'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['content']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""
                    elif (key == 'urls'):
                        try:
                            dict_result['social_'+str(key) + '_' + str(i + 1)] = data_inner[key][i]['url']
                        except:
                            dict_result[str(key) + '_' + str(i + 1)] = ""

        dict_result = OrderedDict(sorted(dict_result.items(), key=lambda i:custom_order_list_keys.index(i[0])))

        list_inner_data.append(dict_result)

    dict_batch_data['data'].append(list_inner_data)
    json_result = json.dumps(dict_batch_data)

    # with open('data.json', 'w') as outfile:
    #     json.dump(dict_batch_data, outfile)
    return json_result


def custom_order_key(dict_header):
    custom_order_key = list()
    custom_order_key.append('names')
    for i in range(dict_header['emails']):
        custom_order_key.append('emails_'+str(i+1))
    for i in range(dict_header['images']):
        custom_order_key.append('images_' + str(i+1))
    for i in range(dict_header['usernames']):
        custom_order_key.append('usernames_' + str(i+1))
    for i in range(dict_header['phones']):
        custom_order_key.append('phones_' + str(i+1))
    custom_order_key.append('gender')
    custom_order_key.append('age')
    for i in range(dict_header['languages']):
        custom_order_key.append('languages_' + str(i+1))
    for i in range(dict_header['addresses']):
        custom_order_key.append('addresses_' + str(i+1))
    for i in range(dict_header['jobs']):
        custom_order_key.append('jobs_' + str(i+1))
    for i in range(dict_header['educations']):
        custom_order_key.append('educations_' + str(i+1))
    for i in range(dict_header['relationships']):
        custom_order_key.append('relationships_' + str(i+1))
    for i in range(dict_header['user_ids']):
        custom_order_key.append('user_ids_' + str(i+1))
    for i in range(dict_header['urls']):
        custom_order_key.append('social_urls_' + str(i+1))
    custom_order_key.append('appID')



    return (custom_order_key)

if __name__ == '__main__':
	lambda_handler(event=True, context=False)
