import json
import csv
import pandas as pd
from collections import OrderedDict
from pandas.io.json import json_normalize

from collections import defaultdict
import copy


## main : it controls all logics
def main():
    json_result = data_fetch_json("json_person.json")
    convert_json_to_csv(json_result)
    # print(json_result)
    # print("------------------------------------------------")


##Logic : [Parent key as batch key{array of inner dictionary contains keys of respective person as array(inner_dict)}]

## Fetching Json from given bulk Json data
def data_fetch_json(path):
    dict_batch_data = {}
    list_inner_data = list()
    dict_batch_data['data'] = list()

    data = pd.read_json(path)

    data_inner = data['person'] ## data_inner stores everythings about person names, emails, jobs......
    #
    data_inner_keys = data_inner[0].keys()  ## fetching all keys of data

    #
    dict_header = {}  ## initializing header for keys in json
    #
    for header in data_inner_keys:
        dict_header[header] = 0  ## initializing the dictionary to 0
    #
    length_batch_data = len(data)

    #
    # ##Fetching all keys from dictionary because they are unique for all person
    for i in range(length_batch_data):
        data_inner = data['person'][i]

    #     ## Finding the maximum count of each key from whole bunch of json for all person's data
        for key, value in data_inner.items():
            length_key_data = 0
            try:
                if (type(data_inner[key]) is list):
                    length_key_data = len(data_inner[key])
                elif (type(data_inner[key]) is str):
                    length_key_data = 1
            except:
                length_key_data = 0

            if (dict_header[key] <= length_key_data):
                dict_header[key] = length_key_data


    # ## creating a dictionary with renaming the keys with no of occurance of the keys from (1-n) ex: job:{job_1,job_2,job_3,job_4...job_n}
    dict_header_items = dict()
    for key, value in dict_header.items():
        if(key=='urls'):
            for i in range(value):
                dict_header_items['social_'+str(key) + '_' + str(i + 1)] = ""
        elif(key == 'names'or key == 'dob' or key == 'gender' or key == 'UniqueId' or key == 'origin_countries'):
            dict_header_items[key]=""
        else:
            for i in range(value):
                dict_header_items[str(key) + '_' + str(i + 1)] = ""
            if(value == 0):
                dict_header_items[key] = ""

    #
    # ## we have three different exception in set of dictionaries keys name,gender,dob which are unique and cannot be multiple
    #
    for i in range(length_batch_data):

        dict_result_final = copy.deepcopy(dict_header_items) ## deep copy allows us to create new copy of dictionary(we need this copy coz tradiotnal copy methods also overwrite the original dictionary with copied object coz its on same reference location)
        dict_result_final.pop('ethnicities')
        dict_result_final.pop('appID')
        dict_result_final.pop('origin_countries')

        dict_result_final['age'] = dict_result_final.pop('dob')

        dict_result_final['names'] = ''
        dict_result_final['gender'] = ''


    #
        dict_result_final = data['person'][i]

    #

        for key, value in data_inner.items():
            dict_result = copy.deepcopy(dict_result_final)

            if (key == 'names'):

                try:

                    if(('last' not in data_inner[key][0].keys())):
                        dict_result['names'] = data_inner[key][0]['first']
                    elif(('first' not in data_inner[key][0].keys())):
                        dict_result['names'] = data_inner[key][0]['last']
                    else:
                        dict_result['names'] = data_inner[key][0]['first'] + " " + data_inner[key][0]['last']

                except:

                    del dict_result[key]


            elif (key == 'dob'):
                try:
                    dict_result['age'] = data_inner['dob']['display']
                except:
                    del dict_result[key]


            elif (key == 'gender'):
                try:

                    dict_result['gender'] = data_inner['gender']['content']
                except:
                    del dict_result[key]

            elif (key == 'UniqueId'):
                try:
                    dict_result['UniqueId'] = data_inner['UniqueId']
                except:
                    del dict_result[key]
            else:
                len_key = 0
                try:
                    if(type(data_inner[key]) is list):
                        len_key = len(data_inner[key])
                    elif(type(data_inner[key]) is str):
                        len_key = 1
                except:
                    len_key = 0
                for i in range(len_key):
                    if(key=='emails'):
                        try:
                            dict_result[str(key)+'_'+str(i+1)] = data_inner[key][i]['emails']
                        except:
                            del dict_result[key]
                    elif(key=='images'):
                        try:
                            dict_result[str(key)+'_'+str(i+1)] = data_inner[key][i]['url']
                        except:
                            del dict_result[key]
                    elif (key == 'usernames'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['content']
                        except:
                            del dict_result[key]
                    elif (key == 'phones'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display_international']
                        except:
                            del dict_result[key]
                    elif (key == 'languages'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'addresses'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'addresses'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'jobs'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'educations'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'relationships'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['names'][0]['display']
                        except:
                            del dict_result[key]
                    elif (key == 'user_ids'):
                        try:
                            dict_result[str(key) + '_' + str(i + 1)] = data_inner[key][i]['content']
                        except:
                            del dict_result[key]
                    elif (key == 'urls'):
                        try:
                            dict_result['social_'+str(key) + '_' + str(i + 1)] = data_inner[key][i]['url']
                        except:
                            del dict_result[key]


        if(len(dict_result) > 0):
            print(dict_result)
            dict_result = OrderedDict(sorted(dict_result.items()))
        list_inner_data.append(dict_result)

    dict_batch_data['data'].append(list_inner_data)
    json_result = json.dumps(dict_batch_data)


    # with open('data.json', 'w') as outfile:
    #     json.dump(dict_batch_data, outfile)
    return json_result

def convert_json_to_csv(json_result):

    json_r = json.loads(json_result, object_pairs_hook=OrderedDict)

    # open a file for writing

    person_data = open('Person_data_test.csv', 'w')
    # create the csv writer object

    csvwriter = csv.writer(person_data)

    count = 0

    for person in json_r['data'][0]:


        if count == 0:
            header = person.keys()

            csvwriter.writerow(header)

            count += 1

        csvwriter.writerow(person.values())

    person_data.close()


if __name__ == '__main__':
    main()
